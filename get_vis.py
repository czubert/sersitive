'''
Prepares data for visualisation separately from streamlit.
Gets from data from spectrometer and after processing saves it as a file.
'''

from processing import separate
from opt import save_read

url = '../data/*/*.txt'

separated_metadata_data_dfs, names_lower, split_names, not_assigned_spectra = \
    separate.separate_type(url, 'visualisation')

# Getting data for visualisation
data_for_vis = {'separated_metadata_data_dfs': separated_metadata_data_dfs, 'names_lower': names_lower,
                'split_names': split_names, 'not_assigned_spectra': not_assigned_spectra}


# Saving data for visualisation
path = 'visualisation/data_for_vis'
file_name = 'vis_file'

print(data_for_vis)

save_read.save_as_joblib(data_for_vis, file_name, path)
