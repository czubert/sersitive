"""
Application responsible for training estimators based on data available
"""
import time
from datetime import datetime

from processing import separate, label_spectra, processing_ml
from ML_model import split, train_estimators

url = 'data_input/data/*/*.txt'

now = datetime.now()

print(f'Starting time: {now.strftime("%Y_%m_%d %H:%M:%S")}')
print()

# Loading data, and separating it to metadata and data
start_time = time.time()
print('Separating data...')
separated_metadata_data_dfs = separate.check_if_data_separated(url)
print(f'Data loaded in {round(time.time() - start_time, 2)} seconds')
print()

# TODO Why function below always give error? (for now just run program for the second time)
# Preparing data for ML
start_time = time.time()
print('Preparing data for ML...')
processing_ml.check_if_processed(separated_metadata_data_dfs)
print(f'ML data prepared in {round(time.time() - start_time, 2)} seconds')
print()

# Runs module responsible for rating spectra quality
start_time = time.time()
print('Labeling data quality...')
labeled_data_for_ml = label_spectra.rate_spectra()
print(f'Quality labeled in {round(time.time() - start_time, 2)} seconds')
print()

# Splitting data using train_test_split
start_time = time.time()
print('Splitting data...')
split_data = split.splitting_data(labeled_data_for_ml)
print(f'Data split in {round(time.time() - start_time, 2)} seconds')
print()

# Training models
start_time = time.time()
print('Training models...')
for key in split_data.keys():
    train_estimators.train_estimators(split_data[key], key)

print()
print()
print(f'All models trained in {round(time.time() - start_time, 2)} seconds')
