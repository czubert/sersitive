import pandas as pd
import re
import numpy as np

# Models
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegressionCV
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB

# Best params
from sklearn.model_selection import GridSearchCV

# Results visualisation
from sklearn.metrics import confusion_matrix


# Support Vector Machine
def svc_optimisation(train_test_data):
    """
    Takes tuple of DataFrames consists of X, y train and test data. Sets parameters for SVC estimator
    and calls a function searching for best parameters for this data and estimator.
    :param train_test_data: tuple
    :return: returns return of set_fit_model function - description of estimator and ML_model
    """
    params = {
        # 'kernel': ['linear', 'rbf', 'sigmoid', 'poly'],  # linear
        # 'kernel': ['linear'],
        'kernel': ['poly', 'rbf', 'sigmoid'],
        'C': [0.1, 1.0, 10],
        'degree': [1, 5, 10],  # 1
        # 'gamma': ['scale', 'auto'],  # scale
        # 'coef0': [0.0001, 0.001, 0.01],  # I don't know if going to such small numbers is good for ML_model
        # 'shrinking': [True, False],  # True
        # 'tol': [0.0001, 0.001, 0.01],  # I don't know if going to such small numbers is good for ML_model
        # 'verbose': [True, False],  # True
        # 'decision_function_shape': ['ovo', 'ovr'],  # ovo
        # 'max_iter': [10, 100, 1000, 1000, 10000]  # 50
    }

    return set_fit_model(train_test_data, SVC, params)


# Random Forest
def rf_optimisation(train_test_data):
    """
    Takes tuple of DataFrames consists of X, y train and test data. Sets parameters for RandomForest estimator
    and calls a function searching for best parameters for this data and estimator.
    :param train_test_data: tuple
    :return: returns return of set_fit_model function - description of estimator and ML_model
    :param train_test_data:
    :return:
    """
    params = {'n_estimators': [1, 10, 100],
              'max_depth': [5, 10, 20]
              }
    return set_fit_model(train_test_data, RandomForestClassifier, params)


# Logistic Regression
def lr_optimisation(train_test_data):
    """
    Takes tuple of DataFrames consists of X, y train and test data. Sets parameters for LogisticRegression estimator
    and calls a function searching for best parameters for this data and estimator.
    :param train_test_data: tuple
    :return: returns return of set_fit_model function - description of estimator and ML_model
    :param train_test_data:
    :return:
    """
    params = {'Cs': [1, 10, 100],
              # 'max_iter': [100],
              'penalty': ['l1'],
              'solver': ['liblinear'],
              # 'solver': ['lbfgs'],
              'tol': [0.1],
              'random_state': [100],
              # 'tol': [0.000001, 0.00001, 0.0001],
              'intercept_scaling': [1, 10]
              }
    return set_fit_model(train_test_data, LogisticRegressionCV, params)


#  k-Nearest Neighbors
def process_knn(train_test_data):
    """
    Takes tuple of DataFrames consists of X, y train and test data. Sets parameters for k-Nearest Neighbors estimator
    and calls a function searching for best parameters for this data and estimator.
    :param train_test_data: tuple
    :return: returns return of set_fit_model function - description of estimator and ML_model
    :param train_test_data:
    :return:
    """
    params = {'n_neighbors': [3, 10, 20, 30],
              'leaf_size': [20, 30, 40]}
    return set_fit_model(train_test_data, KNeighborsClassifier, params)


# Bayes
def bayes_optimisation(train_test_data):
    """
    Takes tuple of DataFrames consists of X, y train and test data. Sets parameters for Bayes - GaussianNB estimator
    and calls a function searching for best parameters for this data and estimator.
    :param train_test_data: tuple
    :return: returns return of set_fit_model function - description of estimator and ML_model
    :param train_test_data:
    :return:
    """
    params = {'var_smoothing': [1e-9]}
    return set_fit_model(train_test_data, GaussianNB, params)


def set_fit_model(train_test_data, estimator, params):
    """
    This function is estimating best parameters for different machine learning models.
    It returns data and best parameters of ML_model that was checked - just for information
    :param train_test_data: DataFrame
    :param estimator: Estimator object
    :param params: Dict
    :return: Dict
    """
    est = estimator()
    X_train, X_test, y_train, y_test = train_test_data
    model = GridSearchCV(est, param_grid=params, scoring='accuracy', n_jobs=-1)
    y_train = y_train.astype(int)
    y_test = y_test.astype(int)

    # Model training
    np.random.seed(199)
    model.fit(X_train, y_train)
    y_pred = model.predict(X_test)

    # Show performance of a model
    print(f'Accuracy score: {model.score(X_test, y_test)}')
    print(confusion_matrix(y_test, y_pred))

    est_descr = describe_estimator(model)

    return est_descr, model


def describe_estimator(model):
    """
    Takes ML_model with its best params estimated by GridSearch and creates DataFrame with
    most relevant data: name of estimator, accuracy score and best parameters
    :param model: Estimator ML_model
    :return: DataFrame
    """
    model_name = get_model_name(model)
    idx = model_name

    df = pd.DataFrame(index=[idx])
    df.loc[model_name, 'Accuracy_score'] = model.best_score_

    for key in model.best_params_.keys():
        df.loc[model_name, key] = model.best_params_[key]

    return df


def get_model_name(model):
    """
    Takes ML_model as an argument and returns it's name
    :param model: Estimator ML_model
    :return: String
    """
    model_class = str(model.best_estimator_.__class__)
    model_name = re.findall(r'[A-Z]\w+', model_class)[0]
    return model_name
