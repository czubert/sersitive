'''
Trains estimators separately from streamlit. Gets from data from spectrometer and saves it as a file,
additionally saves processed data as files.
'''

import time
import pandas as pd
from datetime import datetime
import os

from ML_model import estimators
from opt import save_read

ESTIMATOR_LIST = [estimators.rf_optimisation, estimators.process_knn,
                  estimators.svc_optimisation, estimators.lr_optimisation]


# estimators.lr_optimisation

def train_estimators(split_data_for_substr, key):
    """
    Takes split data (train test split data dictionary) as an argument and calls fitting function
    for specified substrate type (Ag, Au)
    """

    save_best_params(find_best_params(split_data_for_substr), key)


def find_best_params(train_test):
    """
    Tests parameters to find best parameters using GridSearch method on all estimators from ESTIMATOR_LIST
    :param train_test: DataFrame
    :param substr_type: Str
    :return:
    """
    df = pd.DataFrame()

    for est in ESTIMATOR_LIST:
        print()
        start_time = time.time()
        tmp_est_name = str(est)[9:]
        tmp_est_name = tmp_est_name[:-22]
        print(f'Now processing: {tmp_est_name}')
        est_description, model = est(train_test)

        est_name = f'{estimators.get_model_name(model)}_model'
        model_path = 'data_output/trained_models'
        save_read.save_as_joblib(model, est_name, model_path)
        df = pd.concat([df, est_description], sort=False)
        print(f'{est_name}: trained in {round(time.time() - start_time, 2)} seconds')
    return df


def save_best_params(df, substr_type='other'):
    """
    Takes best ML_model data for each estimator and type of substrate as arguments and saves data to file.
    :param df: DataFrame
    :param substr_type: Str
    """
    now = datetime.now()
    new_dir_name = now.strftime("%Y_%m_%d-%H-%M")
    path = 'data_output/models_best_params'

    if not os.path.isdir(f'{path}'):
        os.mkdir(f'{path}')

    save_read.save_as_csv(df, f'Best_params_of_{substr_type}', f'{path}/{new_dir_name}')

# """
# This part checks the correlation between ag and au models and data.
# Model is fitted on ag or au as training data and accordingly au or ag as test data.
# """
#
#
# def get_x(df, substr_type):
#     """
#     Manually divided data to get only x_train
#     :param substr_type: Str
#     :return: DataFrames
#     """
#     return df[substr_type].iloc[:, :-4]
#
#
# def get_y(df, substr_type):
#     """
#     Manually divided data to get only y_train
#     :param substr_type: Str
#     :return: DataFrames
#     """
#     return df[substr_type].Quality
#
#
# """
# This part is responsible for checking the cross validation scores for all the estimators used in the application.
# """
#
#
# def validate_estimators(cv):
#     """
#     This function calls validate (cross_val_score) function for both types of substrates and all estimators
#     used in application
#     :param cv: Bool
#     """
#     for estimator in ESTIMATOR_LIST:
#         validate(estimator, 'ag', cv)
#         validate(estimator, 'au', cv)
#         pipe_application('ag')
#         pipe_application('au')
#
#
# def validate(estimator, substr_type, cv=5):
#     """
#     Takes estimator and substrate type as arguments and calls cross_val_score method for each estimator
#     and prints accuracy scores for each
#     :param estimator: sklearn estimator
#     :param substr_type: Str
#     :param cv: Bool
#     """
#     if cv:
#         cv = ShuffleSplit(n_splits=10, test_size=0.3)
#
#     if estimator == ESTIMATOR_LIST[2]:  # LogisticRegression for default arguments returns errors - solved
#         model = estimator(solver='liblinear')
#     else:
#         model = estimator()
#
#     X = get_x(substr_type)
#     y = get_y(substr_type)
#
#     scores = cross_val_score(model, X, y, cv=cv, scoring='f1_macro')
#     print(f'Substrate: {substr_type}, Estimator: {model.__class__}, Scores: {scores}')
#
#
# """
# Trying to use pipeline
# """
#
#
# def pipe_application(substr_type):
#     pipe = Pipeline([
#         # the reduce_dim stage is populated by the param_grid
#         ('reduce_dim', 'passthrough'),
#         ('classify', LinearSVC(dual=False, max_iter=10000))
#     ])
#
#     N_FEATURES_OPTIONS = [2, 4, 8]
#     C_OPTIONS = [1, 10, 100, 1000]
#     param_grid = [
#         {
#             'reduce_dim': [PCA(iterated_power=7)],
#             'reduce_dim__n_components': N_FEATURES_OPTIONS,
#             'classify__C': C_OPTIONS
#         },
#         # {
#         #     'reduce_dim': [SelectKBest(chi2)],
#         #     'reduce_dim__k': N_FEATURES_OPTIONS,
#         #     'classify__C': C_OPTIONS
#         # },
#     ]
#
#     grid = GridSearchCV(pipe, n_jobs=1, param_grid=param_grid)
#     X = get_x(labeled_data_for_ml, substr_type)
#     y = get_y(labeled_data_for_ml, substr_type)
#     grid.fit(X, y)
#     mean_scores = np.array(grid.cv_results_['mean_test_score'])
#     print(mean_scores)
#
#
# """
# Calling blocks of functions
# """
#
# if __name__ == '__main__':
#     # calling function responsible for calculating cross-validation scores
#     validate_estimators(cv=True)
