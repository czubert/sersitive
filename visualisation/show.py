import streamlit as st
import visualisation

AG = 'ag'
AU = 'au'
AGB = 'tlo ag'
AUB = 'tlo au'

DESCRIPTIONS = {
    AG: "Spectral summary for PMBA on Silver Substrates",
    AU: "Spectral summary for PMBA on Silver-Gold Hybrid Substrates",
    AGB: "Spectral summary for Silver Substrates Background",
    AUB: "Spectral summary for Silver-Gold Hybrid Substrates Background",
}


# def show_grouped_spectra(grouped_std_dfs, y_grouped_value):
def show_spectra(grouped_std_dfs, y_value):
    # y_value = 'Dark Subtracted #1'

    labels = {AG: 'Show Ag grouped spectra',
              AU: 'Show Au grouped spectra',
              AGB: 'Show Ag grouped background spectra',
              AUB: 'Show Au grouped background spectra'}

    labels_mean = {AG: 'Show Ag mean spectra',
                   AU: 'Show Au mean spectra',
                   AGB: 'Show Ag mean background spectra',
                   AUB: 'Show Au mean background spectra'}

    btn_labels = {AG: 'Ag',
                  AU: 'Au',
                  AGB: 'Ag background',
                  AUB: 'Au background'}

    def show(label, substrate_type):
        if st.sidebar.checkbox(label):
            descr = DESCRIPTIONS[substrate_type]

            fig = visualisation.draw.draw_plot(grouped_std_dfs[substrate_type], descr, y_value)

            st.plotly_chart(fig)

            if st.button(f'Show {btn_labels[substrate_type]} data'):
                st.dataframe(grouped_std_dfs[substrate_type])

    for key in grouped_std_dfs.keys():
        if y_value == 'Dark Subtracted #1':
            show(labels[key], key)
        else:
            show(labels_mean[key], key)


def show_ml_data_frames(dfs):
    for key in dfs.keys():
        st.write(dfs[key])
