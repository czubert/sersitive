import plotly.express as px
import plotly.graph_objects as go

import streamlit as st
import pandas as pd



@st.cache
def draw_plot(df_for_plot, descr='Chosen spectra', y_value='Custom'):
    """
    # dictionary of tuples of dfs separated corresponding to the type of substrate
    :param df_for_plot: DataFrame
    :param descr: String
    :param y_value: String
    :return: Plotly object - Figure
    """
    RS = 'Raman Shift'
    DS = 'Dark Subtracted #1'

    if y_value == 'Average':
        fig = px.line(df_for_plot, x=RS, y=y_value, width=950, height=650,
                      # color_discrete_sequence=px.colors.qualitative.Alphabet, error_y='Average_err')
                      color_discrete_sequence=px.colors.qualitative.Alphabet)

    elif y_value == 'Custom' or y_value == 'all':
        fig = px.line(df_for_plot, x=RS, y=DS, width=950, height=650,
                      # color_discrete_sequence=px.colors.qualitative.Alphabet, error_y='Average_err')
                      color_discrete_sequence=px.colors.qualitative.Alphabet)

    elif y_value == DS:
        corrected_df = pd.melt(df_for_plot, id_vars=df_for_plot.columns[0], value_vars=df_for_plot.columns[1:])
        fig = px.line(corrected_df, x=RS, y='value', color='variable', width=950, height=650,
                      color_discrete_sequence=px.colors.qualitative.Alphabet)

    # changing layout and styles
    fig.update_layout(title=descr, template='plotly_white',
                      xaxis_title=f"{RS} [cm^-1]",
                      yaxis_title="Intensity",
                      legend=go.layout.Legend(x=0.5, y=-0.2, traceorder="normal",
                                              font=dict(
                                                  family="sans-serif",
                                                  size=10,
                                                  color="black"
                                              ),
                                              bgcolor="#fff",
                                              bordercolor="Black",
                                              borderwidth=None,
                                              orientation='h',
                                              xanchor='auto',
                                              itemclick='toggle',

                                              ))

    return fig
