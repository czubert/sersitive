import streamlit as st
import numpy as np

from processing import separate
from visualisation import draw
from visualisation import show
from opt import utils

RS = 'Raman Shift'
DFS = {'ML model grouped spectra': 'Dark Subtracted #1', 'ML model mean spectra': 'Average'}


def show_plot(display_options_radio, denominator=1, key=None):
    """
    Runs functions responsible for getting file from user and showing it in streamlit app
    :param denominator: Int
    :param display_options_radio: String
    :param key: String
    """
    uploaded_file = utils.upload_file()

    show_custom_plot(uploaded_file, denominator, display_options_radio, key=key)


def show_custom_plot(uploaded_files, denominator, display_options_radio, key):
    """
    Based on uploaded files and denominator it shows either single plot of each spectra (file),
    all spectra on one plot or spectra of mean values
    :param uploaded_files: File
    :param denominator: Int
    :param display_options_radio: String
    :param key: String
    :return:
    """
    if uploaded_files is not None:
        temp_data_df, temp_meta_df = utils.read_data_metadata(uploaded_files)
        # read_data_metadata_alternative(uploaded_files)
        df = utils.group_dfs(temp_data_df)

        if display_options_radio == 'All uploaded spectra':
            for col in range(len(df.columns)):
                df2 = df.copy()
                df2.reset_index(inplace=True)

                st.write(draw.draw_plot(df2.iloc[:, [0, col + 1]], y_value='Custom'))
                show_data_metadata(temp_meta_df, temp_data_df, col + 1)

        elif display_options_radio == 'All uploaded spectra on one plot':
            # changing columns names, so they are separated on the plot,
            # before all columns had the same name
            df.columns = np.arange(len(df.columns))
            # drawing the plot
            st.write(draw.draw_plot(df, y_value='Dark Subtracted #1'))
            utils.show_dataframe(df, key)

        if display_options_radio == 'Mean of uploaded spectra':
            df['Average'] = df.mean(axis=1)
            df2 = df.copy()
            df2.reset_index(inplace=True)
            st.write(draw.draw_plot(df2, y_value='Average'))
            utils.show_dataframe(df2, key)


def show_data_metadata(meta, data, no):
    """

    :param meta:
    :param data:
    :param no:
    :return:
    """
    important_idx = ['intigration times(ms)', 'laser_powerlevel', 'average number', 'time_multiply', 'yaxis_min',
                     'yaxis_max',
                     'xaxis_min', 'xaxis_max', 'interval_time', 'laser_wavelength', 'name']

    if st.button(f'Show data number: {no}'):
        st.dataframe(data[no])

    if st.button(f'Show metadata number: {no}'):
        st.dataframe(meta[no].loc[important_idx, :])


def display_df_on_checkbox(grouped_dfs, description):
    # creates checkboxes in streamlit to visualise data frame on checkbox
    st.sidebar.markdown(description)
    if st.sidebar.checkbox(description):
        for df in grouped_dfs:
            st.write(grouped_dfs[df])


def display_plot_on_checkbox(grouped_dfs, description):
    # creates checkboxes in streamlit to visualise plot on checkbox
    y_value = DFS[description]
    show.show_spectra(grouped_dfs, y_value)


def display_custom_plot():
    st.sidebar.markdown('')
    st.sidebar.markdown('Show plot')

    display_options_radio = st.sidebar.radio(
        "What would you like to see?",
        ('All uploaded spectra', 'All uploaded spectra on one plot', 'Mean of uploaded spectra'))

    # # Slider should reduce features taking meana from every each segment determined by user in slider
    # denominator = st.sidebar.slider(label='Reducing features by getting mean', min_value=1, max_value=20, step=1)
    # show_plot(display_options_radio, denominator)

    show_plot(display_options_radio)
