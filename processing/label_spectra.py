'''
Gets processed data separately from streamlit.
Data is processed by 'train_estimators.py'
Gets data from file saved by train_estimators and labels spectra.
'''
import numpy as np

from opt import save_read

SUBSTRATE_TYPES = ['ag', 'au']
LP = 'laser_power'
IT = 'integration_time'
FN = 'File_name'


def rate_spectra():
    bg_spectra = dict()
    pmba_spectra = dict()

    for substrate in SUBSTRATE_TYPES:
        # reading data
        df = save_read.read_csv(substrate, 'data_output/tmp_processed_data')

        # changing types of spectra (background or not) to binary numbers
        df.loc[df['label'] == f'tlo {substrate}', 'label'] = 1
        df.loc[df['label'] == f'{substrate}', 'label'] = 0

        # changing name of column with background spectra from 'label' to 'BG'
        df.rename(columns={'label': 'BG', "Unnamed: 0": FN}, inplace=True)
        df['BG'].astype(int)

        # Add substrate ID to each measurement
        df.loc[:, 'id'] = df[FN].map(lambda x: x[:7])

        # Getting rid of spectra with names not starting from 's'
        df = df[df['id'].str.match('^[Ss].*')]

        # Unifying id's
        df2 = df.id.str.split('_', expand=True, n=1)
        df2.rename(columns={0: 'a', 1: 'b'}, inplace=True)
        df2['b'] = df2.b.str.replace(' ', '')
        df2['b'] = df2.b.str.replace('_', '')

        df2 = df2.dropna(axis=0)
        mask = df2.loc[:, 'b'].str.len() == 2
        df2.loc[mask, 'b'] = '0' + df2.loc[:, 'b']  # .astype(str)  # add zeros if

        # df2 = df2[df2['b'].notna()]
        df['id'] = df2['a'] + '_' + df2['b']
        # df['id'] =  df2[['a', 'b']].agg('_'.join, axis=1)  # join - #TODO does it work properly - id's matches?
        df.dropna(subset=['id'], inplace=True)

        # Getting rid of wrong named spectra
        mask = ~df['id'].str.contains('ag')
        # df['id'] = df.loc[mask, 'id']
        # df.dropna(axis=0, inplace=True)
        df = df[mask]

        # Creating DataFrames containing divided spectra into background and pmba
        pmba = df[df['BG'] == 0].copy()
        bg = df[df['BG'] == 1].copy()

        # # TODO needs improvements
        # # Set background quality based on max value from pmba quality
        pmba = estimate_pmba_enhancement(pmba)  # Estimating quality based on quartiles
        bg = assign_quality_to_bg2(bg, pmba)  # Setting bg quality based on pmba quality

        # This pmba part is only temporary until separation will not work well
        pmba = pmba[pmba['Quality'].notna()]
        pmba_spectra[substrate] = pmba

        bg = bg[bg['Quality'].notna()]
        bg_spectra[substrate] = bg

    # return pmba_spectra
    return bg_spectra  #TODO not working yet, separation needs improvements


def estimate_pmba_enhancement(df):
    peak_names = []

    # Parameters where to look for the peaks that define the spectra
    peaks = {
        'start_peak': ['703.6', '829.34', '1005.02'],
        'end_peak': ['716.53', '839.85', '1011.1'],
        'start_bg': ['692.8', '808.25', '970.39'],
        'end_bg': ['733.72', '879.59', '1033.35'],
    }

    # WHAT Splitting it into Ag and Au has any sense?
    # WHAT Also splitting into PMBAaq and PMBAOH, also splitting to plasma/non plasma

    for el in range(len(peaks['start_peak'])):
        peak = get_max_peak(df, peaks['start_peak'][el], peaks['end_peak'][el])
        bg = get_min_peak(df, peaks['start_bg'][el], peaks['end_bg'][el])

        peak_num = f'peak{el}'
        df.loc[:, peak_num] = (peak - bg)
        peak_names.append(peak_num)

    it = [4, 2, 1]
    lp = [20, 15, 10, 5, 3, 2, 1]

    for integr_time in it:
        for power in lp:
            df.loc[((df[IT] == integr_time) & (df[LP] == power)), 'Quality'] = rate_peak(df, peak_names, power,
                                                                                         integr_time)

    # df = df[df['Quality'].notna()]

    return df


def get_max_peak(df, start, end):
    return df.loc[:, start:end].max(axis=1)


def get_min_peak(df, start, end):
    return df.loc[:, start:end].min(axis=1)


def rate_peak(df, peak_names, power, integr_time):
    tmp_df = df[(df[LP] == power) & (df[IT] == integr_time)].copy()

    quality_peak_names = []

    for peak_name in peak_names:
        quantiles = tmp_df[peak_name].quantile([0.25, 0.45, 0.50, 0.75])
        quality_name = f'Quality-{peak_name}'
        quality_peak_names.append(quality_name)

        tmp_df[quality_name] = np.NaN

        # For Quality == 0
        tmp_df.loc[tmp_df.loc[:, peak_name] <= quantiles[0.45], quality_name] = 0

        # For Quality == 1
        tmp_df.loc[tmp_df[peak_name] > quantiles[0.45], quality_name] = 1

        # For Quality == 2
        tmp_df.loc[tmp_df[peak_name] >= quantiles[0.50], quality_name] = 2

        # For Quality == 3
        tmp_df.loc[tmp_df[peak_name] > quantiles[0.75], quality_name] = 3

    # important returns statistics of peak (max, median, mean)
    return round(tmp_df.loc[:, quality_peak_names].max(axis=1))


def assign_quality_to_bg(bg, pmba):
    pmba = pmba.loc[:, ['id', 'Quality']]

    # important returns statistics for quality of substrate (max, median, mean)
    pmba = pmba.groupby(['id'], sort=True, as_index=True)['Quality'].max()

    bg = bg.copy()
    bg['Quality'] = np.NaN

    bg.set_index('id', inplace=True)

    for pmba_row in pmba.iteritems():
        for bg_row in bg.iterrows():
            if pmba_row[0] == bg_row[0]:
                bg.loc[bg_row[0], 'Quality'] = pmba_row[1]

    bg = bg[bg['Quality'].notna()]

    return bg


def assign_quality_to_bg2(bg, pmba):
    df = pmba.loc[:, ['id', 'Quality']].copy()
    df_bg = bg.copy()

    df = df.groupby(['id'], sort=True, as_index=False)['Quality'].max()

    df_bg.set_index('id', inplace=True)
    df = df[df['Quality'].notna()]
    df.set_index('id', inplace=True)

    df_bg['Quality'] = np.NaN

    for pmba_row in df.iterrows():
        for bg_row in df_bg.iterrows():
            if pmba_row[0] == bg_row[0]:
                df_bg.loc[bg_row[0], 'Quality'] = pmba_row[1][0]

    return df_bg
