import streamlit as st

from opt import rsd
from opt import utils

RSD = 'Calculate RSD'
EF = 'Calculate EF'


# TODO przejrzeć ten kod i zobaczyć co ma zostać, a co jest do wywalenia

def estimate():
    st.sidebar.markdown('')
    st.sidebar.text('Estimate substrates quality \nbased on uploaded spectra')

    uploaded_files = utils.upload_file()

    if uploaded_files is not None:
        temp_data_df, temp_meta_df = utils.read_data_metadata(uploaded_files)
        # read_data_metadata_alternative(uploaded_files)
        df = utils.group_dfs(temp_data_df)

        display_options_radio = st.sidebar.radio("What would you like to do?", (RSD, EF))

        if display_options_radio == RSD:
            peak1 = df.loc[680:725, :]
            peak2 = df.loc[990:1010, :]
            bg = df.loc[938:941, :]

            rsd.rsd(peak1, peak2, bg)

        elif display_options_radio == EF:
            pass


def determine_if_bg():
    pass
