import pandas as pd
import os

from opt import save_read

"""
Module responsible for processing data for machine learning  
"""

SUBSTRATE_TYPES = ['ag', 'au']


def group_dfs(separated_metadata_data_dfs):
    """
    Returns dict consists of one DataFrames per data type, other consists of mean values per data type.
    :param separated_metadata_data_dfs: dict
    :return: dict
    """
    # groups Dark Subtracted column from all dfs to one and overwrites data df in dictionary
    grouped_df = {}

    for substrate_type in separated_metadata_data_dfs.keys():
        data_list = []
        for metadata_data in separated_metadata_data_dfs[substrate_type]:
            tmp_meta = metadata_data[0]
            tmp_data = metadata_data[1]
            tmp_data.loc['laser_power'] = tmp_meta.loc['laser_powerlevel'][1]
            tmp_data.loc['integration_time'] = int(tmp_meta.loc['intigration times(ms)'][1]) * \
                                               int(tmp_meta.loc['time_multiply'][1]) / 1000
            data_list.append(tmp_data)

        df = pd.concat(data_list, axis=1, sort=False).T

        grouped_df[substrate_type] = df

    return grouped_df


def label_features(dfs):
    """
    Takes dictionary with Data Frames with spectra of background or analytes based on analyte type and labels inplace
    spectra as it's keys (ag, au, ag_bg, au_bg)
    :param dfs: Dict
    """
    for key in dfs.keys():
        dfs[key].dropna(axis=0, inplace=True, how='any')
        dfs[key]['label'] = key


def concat_type_with_bg(dfs):
    """
    Takes dictionary of dfs and concatenates background spectra with analyte spectra depending on substrate type (ag/au)
    :param dfs: Dict
    :return: Dict
    """
    ag_list = []
    au_list = []

    for key in dfs.keys():
        if 'ag' in key:
            ag_list.append(dfs[key])
        elif 'au' in key:
            au_list.append(dfs[key])
    ag = pd.concat(ag_list, sort=False)
    print(f'Nan in Ag DataFrame: {ag.isna().sum().sum()}')
    au = pd.concat(au_list, sort=False)
    print(f'Nan in AgAu DataFrame: {au.isna().sum().sum()}')

    data_for_ml = {'ag': ag, 'au': au}

    return data_for_ml


def preparing_data_for_ml(separated_metadata_data_dfs):
    """
    Takes Data Frames with separated data and metadata in case of substrate type and saves DataFrames as files.
    DataFrames contains labeled features for background + analyte spectra for each type of substrate (ag and au) -
    data_for_ml.
    :param separated_metadata_data_dfs: DataFrame
    """
    grouped_dfs = group_dfs(separated_metadata_data_dfs)

    label_features(grouped_dfs)

    data_for_ml = concat_type_with_bg(grouped_dfs)
    path_name = 'data_output/tmp_processed_data'

    for key in data_for_ml.keys():
        save_read.save_as_csv(data_for_ml[key], key, path_name)


def check_if_processed(separated_metadata_data_dfs):
    if not os.path.isfile(f'data_output/tmp_processed_data/ag.csv'):
        preparing_data_for_ml(separated_metadata_data_dfs)
