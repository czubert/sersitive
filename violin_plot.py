import pandas as pd
import plotly.graph_objects as go

SUBSTRATE_TYPES = ['ag', 'au']
LP = 'laser_power'
IT = 'integration_time'


# TODO make predictions based on violin plots


def rate_spectra():
    data_for_ml = dict()

    for substrate in SUBSTRATE_TYPES:
        # reading data
        data_for_ml[substrate] = read_data(substrate)

        # changing types of spectra (background or not) to binary numbers
        data_for_ml[substrate].loc[data_for_ml[substrate]['label'] == f'tlo {substrate}', 'label'] = 1
        data_for_ml[substrate].loc[data_for_ml[substrate]['label'] == f'{substrate}', 'label'] = 0

        # changing name of column with background spectra from 'label' to 'BG'
        data_for_ml[substrate].rename(columns={'label': 'BG'}, inplace=True)

        # changing name of column with file names
        data_for_ml[substrate].rename(columns={"Unnamed: 0": "File name"}, inplace=True)

        data_for_ml[substrate] = estimate_pmba_enhancement(data_for_ml[substrate], substrate)

    return data_for_ml


def read_data(substrate):
    path = 'data_output/tmp_processed_data'
    return pd.read_csv(f'{path}/{substrate}.csv')


def estimate_pmba_enhancement(df, substrate):
    df = df.copy()
    df = df[df['BG'] == 0]
    peak_names = []

    # Parameters where to look for the peaks that define the spectra
    params = {
        'start_peak': ['705.76', '829.34', '1005.02'],
        'end_peak': ['716.53', '839.85', '1011.1'],
        'start_bg': ['692.8', '808.25', '970.39'],
        'end_bg': ['733.72', '879.59', '1033.35'],
    }

    for el in range(len(params['start_peak'])):
        peak = get_max_peak(df, params['start_peak'][el], params['end_peak'][el])
        bg = get_min_peak(df, params['start_bg'][el], params['end_bg'][el])

        peak_num = f'peak{el}'
        df['Background'] = bg
        df[f'{peak_num}{el}'] = peak
        df[peak_num] = (peak - bg)

        peak_names.append(peak_num)

    for peak_name in peak_names:
        df2, param_names = prepare_df_for_plot(df, peak_name)
        draw_violin_plot(df2, param_names, peak_name, substrate)


def get_max_peak(df, start, end):
    return df.loc[:, start:end].max(axis=1)


def get_min_peak(df, start, end):
    return df.loc[:, start:end].min(axis=1)


def prepare_df_for_plot(df, peak_name):
    # conditions = (df[LP] == 20) & (df[IT] == 4) & (df['BG'] == 0)

    df2 = df.loc[:, [LP, IT, 'BG', peak_name]]

    it = [4, 2, 1]
    lp = [20, 15, 10, 5, 3, 2, 1]
    param_names = []

    for integr_time in it:
        for power in lp:
            conditions = (df[LP] == power) & (df[IT] == integr_time) & (df['BG'] == 0)
            df2.loc[conditions, f'Params'] = f'{power}_{integr_time}'
            param_names.append(f'{power}_{integr_time}')

    return df2[df2['BG'] == 0], param_names


def draw_violin_plot(df, param_names, peak_name, substrate):
    fig = go.Figure()

    for param in param_names:
        fig.add_trace(go.Violin(x=df['Params'][df['Params'] == param],
                                y=df[peak_name][df['Params'] == param],
                                name=param,
                                x0=peak_name,
                                box_visible=True,
                                meanline_visible=True))

    fig.update_layout(title=f'{substrate} substrate {peak_name}', template='plotly_white',
                      xaxis_title=f"Parameters",
                      yaxis_title="Intensity",
                      legend=go.layout.Legend(x=0.5, y=-0.2, traceorder="normal",
                                              font=dict(
                                                  family="sans-serif",
                                                  size=10,
                                                  color="black"
                                              ),
                                              bgcolor="#fff",
                                              bordercolor="Black",
                                              borderwidth=None,
                                              orientation='h',
                                              xanchor='auto',
                                              itemclick='toggle',

                                              ))

    # # Opens plot in a browser
    # fig.show()

    # Saves plot to a html file (appends file)
    with open('data_output/violin_graphs.html', 'a') as f:
        f.write(fig.to_html(full_html=False, include_plotlyjs='cdn'))


if __name__ == '__main__':
    rate_spectra()
