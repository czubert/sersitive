import streamlit as st

import visualisation
import processing
import opt

st.sidebar.title('Menu')

GS = 'ML model grouped spectra'
MS = 'ML model mean spectra'
CUSTOM = 'Upload and see custom plot'
QUALITY = 'Estimate Substrate Quality'

# showing sidebar
display_options_radio = st.sidebar.radio(
    "What would you like to see?",
    ('Data status', GS, MS, CUSTOM, QUALITY), index=0)

vis_path = 'visualisation/data_for_vis'
file_name = 'vis_file'

vis_data = opt.save_read.read_joblib(file_name, vis_path)

if display_options_radio == 'Data status':
    st.title('SERSitive Quality Control')
    progress_bar = st.progress(0)
    status_text = st.empty()

    # takes names of all files in the folder together with folder names,
    # and returns dictionary of split data frames
    status_text.text('Separating data...')
    for i in range(0, 45):
        progress_bar.progress(i)
    status_text.text('Data separated!')

    status_text.text('Grouping data...')
    for i in range(45, 55):
        progress_bar.progress(i)
    # grouped_dfs variable holds dictionary of grouped dfs by the type of substrate
    grouped_dfs = visualisation.processing_vis.group_dfs(vis_data['separated_metadata_data_dfs'])
    for i in range(55, 75):
        progress_bar.progress(i)
    status_text.text('Data grouped!')

    status_text.text('Calculating mean values...')
    for i in range(75, 85):
        progress_bar.progress(i)
    # grouped_dfs variable holds dictionary of grouped dfs by the type of substrate
    mean_std_dfs = visualisation.processing_vis.mean_std(grouped_dfs)
    status_text.text('Mean values calculated')

    # Looking for duplications
    status_text.text('Checking for duplications...')
    processing.separate.duplication_check(vis_data['split_names'], vis_data['names_lower'],
                                          vis_data['not_assigned_spectra'])
    status_text.text('Duplications checked!')

    status_text.text('Optimizing program')
    for i in range(76, 100):
        progress_bar.progress(i)
    status_text.text('Loading successful! Data separated, grouped and mean values were calculated')

elif display_options_radio == GS:
    st.title(GS)
    grouped_dfs = visualisation.processing_vis.group_dfs(vis_data['separated_metadata_data_dfs'])
    visualisation.custom_plot.display_df_on_checkbox(grouped_dfs, display_options_radio)
    visualisation.custom_plot.display_plot_on_checkbox(grouped_dfs, display_options_radio)

elif display_options_radio == MS:
    st.title(MS)
    grouped_dfs = visualisation.processing_vis.group_dfs(vis_data['separated_metadata_data_dfs'])
    mean_std_dfs = visualisation.processing_vis.mean_std(grouped_dfs)
    visualisation.custom_plot.display_df_on_checkbox(mean_std_dfs, display_options_radio)
    visualisation.custom_plot.display_plot_on_checkbox(mean_std_dfs, display_options_radio)

elif display_options_radio == CUSTOM:
    st.title('Uploaded spectra')
    visualisation.custom_plot.display_custom_plot()

elif display_options_radio == QUALITY:
    st.title('Uploaded spectra')
    processing.quality_control.estimate()

# TODO use train models to predict the quality of new substrates based on its bg spectra


print("Streamlit finish it's work")
