**1. Download data to main folder from :**

https://1drv.ms/u/s!AlbmGPIOL6ElhPUZEw2tXsf32wpvOw?e=OZXMxH

**If needed change url names in:**

- visualisation/main.py
- ML_processing/main.py


**2. Prepare data**

- To prepare data for machine learning, and learn models:
Run python script 'ML_processing/main.py'


- To prepare data for visualisation (required for streamlit app - vis.py to work)
visualisation/main.py

**3. Visualise data - run streamlit application 'vis.py'**
   
- first install streamlit library by typing:
   'pip install streamlit'
   or
   'conda install streamlit'
   
   
- then run app by:
   'streamlit run vis.py'


